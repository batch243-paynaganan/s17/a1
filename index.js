// console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
		function printPersonInfo(){
			let personFullName = prompt("Enter your full name:");
			let personAge = prompt("Enter your age:");
			let personLocation = prompt("Enter your location:");

			console.log("I am " + personFullName + ".");
			console.log("I am " + personAge + " years old.");
			console.log("I am from " + personLocation + ".");
		};

		printPersonInfo();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

		function topFiveFavBands(){
			let topFive = "My top 5 favorite bands are:";
			let bandOne = "1.My Chemical Romance";
			let bandTwo = "2.Secondhand Serenade";
			let bandThree = "3. Boys Like Girls";
			let bandFour = "4. Parokya ni Edgar";
			let bandFive = "5. Eraserheads";

			console.log(topFive);
			console.log(bandOne);
			console.log(bandTwo);
			console.log(bandThree);
			console.log(bandFour);
			console.log(bandFive);

		};

		topFiveFavBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
		function topFiveFavMovies(){
			let topFiveMov = "My top 5 favorite movies are:";
			let movieOne = "1. The Intern (2015)";
			let rottenOne = "Rotten Tomatoes Rating: 59%"
			let movieTwo = "2. Avatar (2009)";
			let rottenTwo = "Rotten Tomatoes Rating: 82%"
			let movieThree = "3. Avengers: Endgame (2019)";
			let rottenThree = "Rotten Tomatoes Rating: 94%"
			let movieFour = "4. Transformers (2007)";
			let rottenFour = "Rotten Tomatoes Rating: 58%"
			let movieFive = "5. Zack Snyder's Justice League (2021)";
			let rottenFive = "Rotten Tomatoes Rating: 71%"

			console.log(topFiveMov);
			console.log(movieOne);
			console.log(rottenOne);
			console.log(movieTwo);
			console.log(rottenTwo);
			console.log(movieThree);
			console.log(rottenThree);
			console.log(movieFour);
			console.log(rottenFour);
			console.log(movieFive);
			console.log(rottenFive);
		};

		topFiveFavMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name: "); 
	let friend2 = prompt("Enter your second friend's name: "); 
	let friend3 = prompt("Enter your third friend's name: ");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);
